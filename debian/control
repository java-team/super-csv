Source: super-csv
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Jochen Sprickerhof <jspricke@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 maven-debian-helper (>= 2.1),
 junit4 (>= 4.12),
 libmaven-bundle-plugin-java (>= 3.5.0),
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/java-team/super-csv.git
Vcs-Browser: https://salsa.debian.org/java-team/super-csv
Rules-Requires-Root: no
Homepage: https://github.com/super-csv/super-csv

Package: libsuper-csv-java
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${maven:Depends}
Suggests: ${maven:OptionalDepends}
Description: Super CSV library for Java
 It is highly configurable, and supports reading and writing with POJOs,
 Maps and Lists. It also has support for deep-mapping and index-based
 mapping with POJOs, using the powerful Dozer extension.
 .
 Its flexible 'Cell Processor' API automates data type conversions
 (parsing and formatting Dates, Integers, Booleans etc) and enforces
 constraints (mandatory columns, matching against regular expressions
 etc) - and it's easy to write your own if required.
